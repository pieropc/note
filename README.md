# 常用网址：

[蓝奏云优享版](https://www.ilanzou.com/)

[纯净版系统_Win10纯净版64位_纯净版XP系统-爱纯净官网](http://www.aichunjing.com/)

[果核剥壳 - 互联网的净土](https://www.ghxi.com/)

[软仓丨RuanCang.Net](https://www.ruancang.net/)

[帮小忙，腾讯QQ浏览器在线工具箱平台](https://tool.browser.qq.com/)

[Adobe - Download Adobe Acrobat Reader](https://get.adobe.com/cn/reader/)

[PixPin 截图/贴图/长截图/文字识别/标注 | PixPin 截图/贴图/长截图/文字识别/标注](https://pixpinapp.com/)

[火绒安全](https://www.huorong.cn/)

[Node.js](https://nodejs.org/en)

[Visual Studio Code - Code Editing. Redefined](https://code.visualstudio.com/)

[Layui - 极简模块化前端 UI 组件库(官方文档)](https://layui.dev/)

[Vue.js - 渐进式 JavaScript 框架 | Vue.js](https://cn.vuejs.org/)

[打印机驱动,打印机驱动下载 - 打印机驱动网](https://www.dyjqd.com/)

[AMD 驱动程序与支持 | AMD](https://www.amd.com/zh-hans/support)

[下载新版 NVIDIA 官方驱动](https://www.nvidia.cn/Download/index.aspx?lang=cn)

[腾讯软件中心-海量软件高速下载](https://pc.qq.com/)

[广发证券](https://new.gf.com.cn/softdownload/index)

[华林证券](http://www.chinalions.com/main/AppArea/index.shtml)

[ToDesk远程桌面软件-免费安全流畅的远程连接电脑手机](https://www.todesk.com/download.html)

[Cloud Studio](https://ide.cloud.tencent.com/)

# Window 操作日常

![py](img/py.png)

### 1、Windows 的 “上帝模式”，也被称为 “完全控制面板”

开启方法

- **Windows 7/10/11 通用方法**：在桌面或任意文件夹内的空白处右键单击，选择 “新建”→“文件夹”，然后将新建的文件夹重命名为 “godmode.{ed7ba470-8e54-465e-825c-99712043e01c}”。重命名后，文件夹图标会变为控制面板图标，双击即可打开上帝模式窗口

  ```
  godmode.{ed7ba470-8e54-465e-825c-99712043e01c}
  ```

- **通过运行窗口开启（Windows 10/11）**：按下 “Win+R” 键打开运行窗口，输入 “shell:::{ed7ba470-8e54-465e-825c-99712043e01c}” 后点击 “确定”，可直接打开上帝模式页面

  ```
  shell:::{ed7ba470-8e54-465e-825c-99712043e01c}
  ```

  

### 2、win11 做用win7 查看图片

1. 将文件保存为.reg 格式，如 “restorephotoviewer.reg”。

   ```
   Windows Registry Editor Version 5.00
   [HKEY_CLASSES_ROOT\Applications\photoviewer.dll]
   [HKEY_CLASSES_ROOT\Applications\photoviewer.dll\shell]
   [HKEY_CLASSES_ROOT\Applications\photoviewer.dll\shell\open]
   "MuiVerb"="@photoviewer.dll,-3043"
   [HKEY_CLASSES_ROOT\Applications\photoviewer.dll\shell\open\command]
   @=hex(2):25,00,53,00,79,00,73,00,74,00,65,00,6d,00,52,00,6f,00,6f,00,74,00,25,\
   00,5c,00,53,00,79,00,73,00,74,00,65,00,6d,00,33,00,32,00,5c,00,72,00,75,00,\
   6e,00,64,00,6c,00,6c,00,33,00,32,00,2e,00,65,00,78,00,65,00,20,00,22,00,25,\
   00,50,00,72,00,6f,00,67,00,72,00,61,00,6d,00,46,00,69,00,6c,00,65,00,73,00,\
   25,00,5c,00,57,00,69,00,6e,00,64,00,6f,00,77,00,73,00,20,00,50,00,68,00,6f,\
   00,74,00,6f,00,20,00,56,00,69,00,65,00,77,00,65,00,72,00,5c,00,50,00,68,\
   00,6f,00,74,00,6f,00,56,00,69,00,65,00,77,00,65,00,72,00,2e,00,64,00,6c,\
   00,6c,00,22,00,2c,00,20,00,49,00,6d,00,61,00,67,00,65,00,56,00,69,00,65,\
   00,77,00,5f,00,46,00,75,00,6c,00,6c,00,73,00,63,00,72,00,65,00,65,00,6e,\
   00,20,00,25,00,31,00,00,00
   [HKEY_CLASSES_ROOT\Applications\photoviewer.dll\shell\open\droptarget]
   "CLSID"="{ffe2a43c-56b9-4bf5-9a79-cc6d4285608a}"
   [HKEY_CLASSES_ROOT\Applications\photoviewer.dll\shell\print]
   [HKEY_CLASSES_ROOT\Applications\photoviewer.dll\shell\print\command]
   @=hex(2):25,00,53,00,79,00,73,00,74,00,65,00,6d,00,52,00,6f,00,6f,00,74,00,25,\
   00,5c,00,53,00,79,00,73,00,74,00,65,00,6d,00,33,00,32,00,5c,00,72,00,75,00,\
   6e,00,64,00,6c,00,6c,00,33,00,32,00,2e,00,65,00,78,00,65,00,20,00,22,00,25,\
   00,50,00,72,00,6f,00,67,00,72,00,61,00,6d,00,46,00,69,00,6c,00,65,00,73,00,\
   25,00,5c,00,57,00,69,00,6e,00,64,00,6f,00,77,00,73,00,20,00,50,00,68,00,6f,\
   00,74,00,6f,00,20,00,56,00,69,00,65,00,77,00,65,00,72,00,5c,00,50,00,68,\
   00,6f,00,74,00,6f,00,56,00,69,00,65,00,77,00,65,00,72,00,2e,00,64,00,6c,\
   00,6c,00,22,00,2c,00,20,00,49,00,6d,00,61,00,67,00,65,00,56,00,69,00,65,\
   00,77,00,5f,00,46,00,75,00,6c,00,6c,00,73,00,63,00,72,00,65,00,65,00,6e,\
   00,20,00,25,00,31,00,00,00
   [HKEY_CLASSES_ROOT\Applications\photoviewer.dll\shell\print\droptarget]
   "CLSID"="{60fd46de-f830-4894-a628-6fa81bc0190d}"
   ```

   如果你想在 Windows 11 中使用第三方软件 IrfanView 或 Faststone Image Viewer 来查看图片

   

   ### 3、win11 改 win10 右键菜单

   使用批处理文件

   ```plaintext
   reg add "HKCU\software\classes\clsid\{86ca1aa0-34aa-4e8b-a509-50c905bae2a2}\inprocserver32" /f /ve
   taskkill /f /im explorer.exe&start explorer.exe
   ```

   然后将文档另存为 “win10 右键.cmd”，双击运行该文件即可修改右键菜单为 Win10 样式。如果要恢复为 Win11 右键样式，可以新建另一个文本文档，复制以下内容并另存为 “win11 右键.cmd”，再双击运行210.

   ```plaintext
   reg delete "HKCU\software\classes\clsid\{86ca1aa0-34aa-4e8b-a509-50c905bae2a2}" /f
   taskkill /f /im explorer.exe&start explorer.exe
   ```

   - **注意事项**：运行批处理文件时可能会被杀毒软件误报，需确保文件来源可靠，或者暂时关闭杀毒软件的实时监控功能。

   ### 4、Win11安装的时候，有没有方法跳过“网络连接”和Microsoft账号登录
   ① 按 Shift+F10键，打开命令行程序（部分笔记本选按 Fn+shift+F10）
   ② 进命令行输入： oobe\bypassnro.cmd ，回车 （需要鼠标点下dos窗口，才能输入！）
   ③ 电脑重启再次进入联网界面，下面多出了“ 我没有Internet连接”选项，
   法② 只跳过登录微软账号，正常联网
   随便输入一个不存在的微软账号，eg: no@thankyou.com, 1@1.com ，之后会提示“哎啊，出错了”，即可使用本地账户登录；

   法③ 关闭网络连接流
   ①按 Shift+F10（笔记本电脑可能要按Shift+Fn+F10），
   ② 输入“ taskmgr”；
   ③关闭“网络连接流”进程，即可使用本地账户登录。
    （某些情况下操作一次可能无效，重复多次即可跳过联网

